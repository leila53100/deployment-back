<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/' => [[['_route' => 'index', '_controller' => 'App\\Controller\\DefaultController::index'], null, null, null, false, false, null]],
        '/todo' => [[['_route' => 'getTodo', '_controller' => 'App\\Controller\\DefaultController::getTodo'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_error/(\\d+)(?:\\.([^/]++))?(*:35)'
                .'|/todo/(?'
                    .'|([^/]++)(*:59)'
                    .'|post(*:70)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        35 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        59 => [[['_route' => 'getTodoId', '_controller' => 'App\\Controller\\DefaultController::getTodoId'], ['id'], null, null, false, true, null]],
        70 => [
            [['_route' => 'postTodoId', '_controller' => 'App\\Controller\\DefaultController::postTodo'], [], null, null, false, false, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
